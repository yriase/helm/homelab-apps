{{- define "homelab-apps.argocd-project" -}}
---
apiVersion: argoproj.io/v1alpha1
kind: AppProject
metadata:
  name: {{ . }}
  namespace: argocd
  annotations:
    argocd.argoproj.io/sync-wave: "-1"
spec:
  clusterResourceWhitelist:
  - group: '*'
    kind: '*'
  destinations:
  - namespace: '*'
    server: '*'
  sourceRepos:
  - '*'
{{- end }}

{{- define "homelab-apps.argocd-application" -}}
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: {{.project}}-{{.app}}
  namespace: argocd
spec:
  destination:
    server: https://kubernetes.default.svc
    namespace: {{.project}}
  project: {{.project}}
  source:
    path: charts/{{.project}}/{{.app}}
    repoURL: "{{ .Values.repo }}"
    targetRevision: HEAD
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
{{- end }}